import config from "./libs/config";
import express from "express";
import fileRead from "./libs/films_file_reader";
import favicon from "serve-favicon";
import path from "path";
import getLogger from "./libs/log";
import formidable from "formidable";
import fs from "fs";
import cors from "cors";
import bodyParser from "body-parser";
import * as db from './libs/db_process'
import fileUpload from 'express-fileupload'

let app = express();

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(bodyParser.json());
app.use(fileUpload());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(app.router);

app.options("*",(req,res,next) => {
	res.header("Access-Control-Allow-Origin", req.get("Origin")||"*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
	res.status(200).end();
});

app.use((req, res, next) =>{
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Request-Headers", "*");
	res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, OPTIONS');
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
	res.header("Access-Control-Allow-Credentials", "true");
	next();
});

app.post("/fileupload", (request, response, next) =>{
	let filename = "film.txt";
	let filepath = __dirname + '/uploads/' + filename;
	let req_films = request.files;
	for (let i in req_films) {
		req_films[i].mv(filepath, function (err) {
			if (err)
				return response.status(500).send(err);
			let films = fileRead(filepath);
			films.map((film) => {
				film.save(function (err) {
					if (err) {
						console.log('err');
					} else
						console.log('OK');
				});
			});
			response.send('File uploaded!');
		});
	}
	next();
});

app.get('/films', (request, response, next) =>{
	db.getFilms(request, response, next);
});

app.delete('/films/:id', (request, response, next) =>{
	db.deleteFilm(request, response, next);
});

app.post('/films', (request, response, next) =>{
	db.addFilms(request, response, next);
});

app.use(express);

app.use((req, res) =>{
    res.status(404);
    log.debug('Not found URL: %s',req.url);
    res.send({ error: 'Not found' });
});

app.use((err, req, res, next) =>{
    res.status(err.status || 500);
    log.error('Internal error(%d): %s', res.statusCode, err.message);
    res.send({ error: err.message });
});

app.listen(config.get('port'),() =>{
    console.log('Express server listening on port '+ config.get('port'));
});