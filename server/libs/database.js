import config from './config';
import mongoose from 'mongoose';

mongoose.Promise = global.Promise;
let db = mongoose.createConnection(config.get('mongoose:uri'));

db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
    console.log("Connected!")
});

let StarsSchema = new mongoose.Schema ({
    firstName: {
        type: String,
        required: true
    },
	lastName: {
		type: String,
		required: true
	}
});

let FormatsSchema = new mongoose.Schema ({
    kind: {
        type: String,
        enum: ['VHS', 'DVD', 'Blu-Ray'],
        required: true
    }
});

let FilmsSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
            }, release: {
        type: Number,
        required: true
            },
    stars:  [StarsSchema],
    format: FormatsSchema
});

FilmsSchema.pre('save', function (next) {
	Films.findOne({title: this.title, release: this.release, "format.kind": this.format.kind}, 'title', (err, film) => {
		if (film)
			next(new Error("Film exists"));
		next();
	});
});

export let Stars = db.model("Stars", StarsSchema);
export let Formats = db.model("Formats", FormatsSchema);
export let Films = db.model("Films", FilmsSchema);