import {Films, Formats, Stars} from "./database"

const tryToGetData = (film, str) => {
    str = str.trim().split(":", 2);
    if (str.length === 2) {
        str[0] = str[0].trim().toLowerCase();
        str[1] = str[1].trim();
        if (str[1] !== ""){
            if (str[0] === "title")
                film.title = str[1].toString();
            else if (str[0] === "release year")
                film.release = Number(str[1]);
            else if (str[0] === "format")
                film.format = new Formats({kind :str[1]});
            else if (str[0] === "stars")
				str[1].split(",").map((star) =>{
				    let name = star.trim().split(' ').filter(Boolean);
					film.stars.push(new Stars({firstName: name[0].trim(), lastName: name[1].trim()}));
                });
        }
    }
};

const getFilm = (strFilm) => {
	let film = Films();
    strFilm.trim().split("\n").map((strFilm) => { tryToGetData(film, strFilm);});
    return film;
};

const fileRead = (file) => {
    let fs = require('fs');
    let dataArray = fs.readFileSync(file).toString().trim().split("\n\n").filter(Boolean);
    let films = [];

    for(let i in dataArray) {
        let film = getFilm(dataArray[i]);
        films.push(film);
    }
    return films;
};
export default fileRead;